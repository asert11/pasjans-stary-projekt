#ifndef DECK_H
#define DECK_H

#include "card.h"
#include "console.h"

class deck
{
public:

	deck();
	~deck();
	void Render();
	void AddCard(card * source);
	void InsertCard(card * source, int id);
	void RemoveCard();
	void ClearDeck();
	void NextCard();
	static bool isError();

private:

	card ** table;
	int cardcount;
	SDL_Rect position;
	int actualcardid;
	static SDL_Texture * frontdecktexture;
	static SDL_Texture * backdecktexture;
	static SDL_Texture * lackofcards;
	static bool error;

	SDL_Texture * LoadTexture(std::string file);

	friend class activity;
	friend class backaction;
	friend void Distribute();
};

#endif
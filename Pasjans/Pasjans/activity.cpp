#include "activity.h"

activity::activity()
{
	table = NULL;
	amount = 0;
}

activity::~activity()
{
	delete [] table;
}

void activity::DeckNextCardAction()
{
	if(pdeck->cardcount > 0)
	{
		int hiddencardamount;

		if(pdeck->actualcardid == -1) hiddencardamount = (pdeck->cardcount + 5) / 6;
		else hiddencardamount = (pdeck->cardcount - pdeck->actualcardid + 4) / 6;

		int y = 30;
		int x;

		if(hiddencardamount == 0) x = 16;
		else
		{
			x = 16 + (hiddencardamount - 1) * 2;
		}

		POINT point;
				
		GetCursorPos(&point);
		ScreenToClient(child, &point);
				
		if(point.x >= x && point.x <= x + card::wymiary.w && point.y >= y && point.y <= y + card::wymiary.h)
		{
			if(pdeck->actualcardid != -1) pdeck->table[pdeck->actualcardid]->is_render = false;
					
			pdeck->NextCard();
					
			if(pdeck->actualcardid != -1)
			{
				pdeck->table[pdeck->actualcardid]->is_render = true;
				pdeck->table[pdeck->actualcardid]->hidden = false;

				int showncardamount = pdeck->actualcardid / 6;

				int y = 30;
				int x = 105 + showncardamount * 2;

				pdeck->table[pdeck->actualcardid]->SetActualPos(x, y);
				pdeck->table[pdeck->actualcardid]->SetIdleState(x, y);
			}

			pbackaction->AddAction(backaction::previousdeckcard, 0, 0, backaction::null, backaction::null, 0, 0, 0, 0, 0);
		}
	}
}

void activity::DeckTakeCardAction()
{
	if(pdeck->actualcardid != -1)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(startdragpos.x >= pdeck->table[pdeck->actualcardid]->actual_pos.x && startdragpos.x <= pdeck->table[pdeck->actualcardid]->actual_pos.x + 75 && startdragpos.y >= pdeck->table[pdeck->actualcardid]->actual_pos.y && startdragpos.y <= pdeck->table[pdeck->actualcardid]->actual_pos.y + 104)
		{
			table = new card * [1];
			amount = 1;
			cardfromdeck = true;
			cardfromhandle = false;
			cardfromldeck = false;
			cardid = pdeck->actualcardid;

			table[0] = pdeck->table[pdeck->actualcardid];

			pdeck->RemoveCard();

			if(pdeck->actualcardid != -1)
			{
				pdeck->table[pdeck->actualcardid]->is_render = true;
				pdeck->table[pdeck->actualcardid]->hidden = false;

				int showncardamount = pdeck->actualcardid / 6;

				int y = 30;
				int x = 105 + showncardamount * 2;

				pdeck->table[pdeck->actualcardid]->SetActualPos(x, y);
				pdeck->table[pdeck->actualcardid]->SetIdleState(x, y);
			}
		}
	}
}

void activity::HandleTakeCardAction()
{
	if(handles[0]->cardcount > 0 && amount == 0)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(startdragpos.x >= handles[0]->table[handles[0]->cardcount - 1]->actual_pos.x && startdragpos.x <= handles[0]->table[handles[0]->cardcount - 1]->actual_pos.x + 75 && startdragpos.y >= handles[0]->table[handles[0]->cardcount - 1]->actual_pos.y && startdragpos.y <= handles[0]->table[handles[0]->cardcount - 1]->actual_pos.y + 104)
		{
			table = new card * [1];
			amount = 1;
			cardfromdeck = false;
			cardfromhandle = true;
			cardfromldeck = false;
			handleid = 0;
			cardid = 0;

			table[0] = handles[0]->table[handles[0]->cardcount - 1];

			handles[0]->DeleteCard();
		}
	}

	if(handles[1]->cardcount > 0 && amount == 0)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(startdragpos.x >= handles[1]->table[handles[1]->cardcount - 1]->actual_pos.x && startdragpos.x <= handles[1]->table[handles[1]->cardcount - 1]->actual_pos.x + 75 && startdragpos.y >= handles[1]->table[handles[1]->cardcount - 1]->actual_pos.y && startdragpos.y <= handles[1]->table[handles[1]->cardcount - 1]->actual_pos.y + 104)
		{
			table = new card * [1];
			amount = 1;
			cardfromdeck = false;
			cardfromhandle = true;
			cardfromldeck = false;
			handleid = 1;
			cardid = 0;

			table[0] = handles[1]->table[handles[1]->cardcount - 1];

			handles[1]->DeleteCard();
		}
	}

	if(handles[2]->cardcount > 0 && amount == 0)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(startdragpos.x >= handles[2]->table[handles[2]->cardcount - 1]->actual_pos.x && startdragpos.x <= handles[2]->table[handles[2]->cardcount - 1]->actual_pos.x + 75 && startdragpos.y >= handles[2]->table[handles[2]->cardcount - 1]->actual_pos.y && startdragpos.y <= handles[2]->table[handles[2]->cardcount - 1]->actual_pos.y + 104)
		{
			table = new card * [1];
			amount = 1;
			cardfromdeck = false;
			cardfromhandle = true;
			cardfromldeck = false;
			handleid = 2;
			cardid = 0;

			table[0] = handles[2]->table[handles[2]->cardcount - 1];

			handles[2]->DeleteCard();
		}
	}

	if(handles[3]->cardcount > 0 && amount == 0)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(startdragpos.x >= handles[3]->table[handles[3]->cardcount - 1]->actual_pos.x && startdragpos.x <= handles[3]->table[handles[3]->cardcount - 1]->actual_pos.x + 75 && startdragpos.y >= handles[3]->table[handles[3]->cardcount - 1]->actual_pos.y && startdragpos.y <= handles[3]->table[handles[3]->cardcount - 1]->actual_pos.y + 104)
		{
			table = new card * [1];
			amount = 1;
			cardfromdeck = false;
			cardfromhandle = true;
			cardfromldeck = false;
			handleid = 3;
			cardid = 0;

			table[0] = handles[3]->table[handles[3]->cardcount - 1];

			handles[3]->DeleteCard();
		}
	}
}

void activity::Render()
{
	if(amount == 1) table[0]->Render();
	if(amount > 1)
	{
		for(int i = 0; i < amount; i++)
		{
			table[i]->Render();
		}
	}
}

void activity::event(SDL_Event e)
{
	switch(e.syswm.msg->msg.win.msg)
	{
		case WM_LBUTTONDOWN:
			
			if(react)
			{
				DeckNextCardAction();
				DeckTakeCardAction();
				HandleTakeCardAction();
				LittleTakeCardsAction();
				LittleShowCard();
			}
			break;
	}
}

void activity::Move()
{
	if(amount > 0)
	{
		GetCursorPos(&point);
		ScreenToClient(child, &point);

		if(amount == 1)
		{
			table[0]->actual_pos.x = table[0]->idle_state.x + point.x - startdragpos.x;
			table[0]->actual_pos.y = table[0]->idle_state.y + point.y - startdragpos.y;
		}
		else
		{
			for(int i = 0; i < amount; i++)
			{
				table[i]->actual_pos.x = table[0]->idle_state.x + point.x - startdragpos.x;
				table[i]->actual_pos.y = table[0]->idle_state.y + point.y - startdragpos.y + 16 * i;
			}
		}
	}
}

void activity::DropCard()
{
	if(amount == 1)
	{
		if(table[0]->actual_pos.x >= handles[0]->position.x - 74 && table[0]->actual_pos.x <= handles[0]->position.x + 74 && table[0]->actual_pos.y >= handles[0]->position.y - 103 && table[0]->actual_pos.y <= handles[0]->position.y + 103)
		{
			if(handles[0]->cardcount == 0 && table[0]->value == 0)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[0]->AddCard(table[0]);
				table[0]->SetIdleState(handles[0]->position.x, handles[0]->position.y);
				table[0]->SetActualPos(handles[0]->position.x, handles[0]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[0]->table[handles[0]->cardcount - 1], 1, backaction::handle, addto, 0, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
			else if(handles[0]->cardcount > 0 && table[0]->value == handles[0]->table[handles[0]->cardcount - 1]->value + 1 && table[0]->color == handles[0]->table[handles[0]->cardcount - 1]->color)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[0]->AddCard(table[0]);
				table[0]->SetIdleState(handles[0]->position.x, handles[0]->position.y);
				table[0]->SetActualPos(handles[0]->position.x, handles[0]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[0]->table[handles[0]->cardcount - 1], 1, backaction::handle, addto, 0, addid, idlex, idley, cardid);


				delete [] table;
				table = NULL;
			}
		}
	}

	if(amount == 1)
	{
		if(table[0]->actual_pos.x >= handles[1]->position.x - 74 && table[0]->actual_pos.x <= handles[1]->position.x + 74 && table[0]->actual_pos.y >= handles[1]->position.y - 103 && table[0]->actual_pos.y <= handles[1]->position.y + 103)
		{
			if(handles[1]->cardcount == 0 && table[0]->value == 0)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[1]->AddCard(table[0]);
				table[0]->SetIdleState(handles[1]->position.x, handles[1]->position.y);
				table[0]->SetActualPos(handles[1]->position.x, handles[1]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[1]->table[handles[1]->cardcount - 1], 1, backaction::handle, addto, 1, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
			else if(handles[1]->cardcount > 0 && table[0]->value == handles[1]->table[handles[1]->cardcount - 1]->value + 1 && table[0]->color == handles[1]->table[handles[1]->cardcount - 1]->color)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[1]->AddCard(table[0]);
				table[0]->SetIdleState(handles[1]->position.x, handles[1]->position.y);
				table[0]->SetActualPos(handles[1]->position.x, handles[1]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[1]->table[handles[1]->cardcount - 1], 1, backaction::handle, addto, 1, addid, idlex, idley, cardid);


				delete [] table;
				table = NULL;
			}
		}
	}

	if(amount == 1)
	{
		if(table[0]->actual_pos.x >= handles[2]->position.x - 74 && table[0]->actual_pos.x <= handles[2]->position.x + 74 && table[0]->actual_pos.y >= handles[2]->position.y - 103 && table[0]->actual_pos.y <= handles[2]->position.y + 103)
		{
			if(handles[2]->cardcount == 0 && table[0]->value == 0)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[2]->AddCard(table[0]);
				table[0]->SetIdleState(handles[2]->position.x, handles[2]->position.y);
				table[0]->SetActualPos(handles[2]->position.x, handles[2]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[2]->table[handles[2]->cardcount - 1], 1, backaction::handle, addto, 2, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
			else if(handles[2]->cardcount > 0 && table[0]->value == handles[2]->table[handles[2]->cardcount - 1]->value + 1 && table[0]->color == handles[2]->table[handles[2]->cardcount - 1]->color)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[2]->AddCard(table[0]);
				table[0]->SetIdleState(handles[2]->position.x, handles[2]->position.y);
				table[0]->SetActualPos(handles[2]->position.x, handles[2]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[2]->table[handles[2]->cardcount - 1], 1, backaction::handle, addto, 2, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
		}
	}

	if(amount == 1)
	{
		if(table[0]->actual_pos.x >= handles[3]->position.x - 74 && table[0]->actual_pos.x <= handles[3]->position.x + 74 && table[0]->actual_pos.y >= handles[3]->position.y - 103 && table[0]->actual_pos.y <= handles[3]->position.y + 103)
		{
			if(handles[3]->cardcount == 0 && table[0]->value == 0)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[3]->AddCard(table[0]);
				table[0]->SetIdleState(handles[3]->position.x, handles[3]->position.y);
				table[0]->SetActualPos(handles[3]->position.x, handles[3]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[3]->table[handles[3]->cardcount - 1], 1, backaction::handle, addto, 3, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
			else if(handles[3]->cardcount > 0 && table[0]->value == handles[3]->table[handles[3]->cardcount - 1]->value + 1 && table[0]->color == handles[3]->table[handles[3]->cardcount - 1]->color)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				handles[3]->AddCard(table[0]);
				table[0]->SetIdleState(handles[3]->position.x, handles[3]->position.y);
				table[0]->SetActualPos(handles[3]->position.x, handles[3]->position.y);
				amount = 0;

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &handles[3]->table[handles[3]->cardcount - 1], 1, backaction::handle, addto, 3, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;
			}
		}
	}

	for(int i = 0; i < 7; i++)
	{
		if(amount > 0)
		{
			if(pldeck[i]->cardcount == 0 && table[0]->value == 12 && table[0]->actual_pos.x >= pldeck[i]->PosAndSizes.x - 74 && table[0]->actual_pos.x <= pldeck[i]->PosAndSizes.x + 74 && table[0]->actual_pos.y >= pldeck[i]->PosAndSizes.y - 103 && table[0]->actual_pos.y <= pldeck[i]->PosAndSizes.y + 103)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				pldeck[i]->AddCards(amount, table);

				for(int j = 0; j < amount; j++)
				{
					table[j]->actual_pos.x = pldeck[i]->PosAndSizes.x;
					table[j]->idle_state.x = pldeck[i]->PosAndSizes.x;

					table[j]->actual_pos.y = pldeck[i]->PosAndSizes.y + j * 16;
					table[j]->idle_state.y = pldeck[i]->PosAndSizes.y + j * 16;
				}

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &pldeck[i]->table[pldeck[i]->cardcount - amount], amount, backaction::ldeck, addto, i, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;

				amount = 0;
			}
			else if(pldeck[i]->cardcount > 0 && pldeck[i]->table[pldeck[i]->cardcount - 1]->hidden == false && pldeck[i]->table[pldeck[i]->cardcount - 1]->value == table[0]->value + 1 && pldeck[i]->table[pldeck[i]->cardcount - 1]->color % 2 != table[0]->color % 2 && table[0]->actual_pos.x >= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.x - 74 && table[0]->actual_pos.x <= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.x + 74 && table[0]->actual_pos.y >= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y - 103 && table[0]->actual_pos.y <= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y + 103)
			{
				int idlex;
				int idley;

				table[0]->GetIdleState(idlex, idley);

				for(int j = 0; j < amount; j++)
				{
					table[j]->actual_pos.x = pldeck[i]->PosAndSizes.x;
					table[j]->idle_state.x = pldeck[i]->PosAndSizes.x;

					table[j]->actual_pos.y = pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y + (j + 1) * 16;
					table[j]->idle_state.y = pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y + (j + 1) * 16;
				}

				pldeck[i]->AddCards(amount, table);

				backaction::addremove addto;
				int addid;

				if(cardfromdeck)
				{
					addto = backaction::deck;
					addid = 0;
				}

				if(cardfromhandle)
				{
					addto = backaction::handle;
					addid = handleid;
				}

				if(cardfromldeck)
				{
					addto = backaction::ldeck;
					addid = ldeckid;
				}
				
				pbackaction->AddAction(backaction::returncard, &pldeck[i]->table[pldeck[i]->cardcount - amount], amount, backaction::ldeck, addto, i, addid, idlex, idley, cardid);

				delete [] table;
				table = NULL;

				amount = 0;
			}
		}
	}

	if(amount > 0) ReturnCard();
}

void activity::ReturnCard()
{
	if(cardfromdeck && amount > 0)
	{
		if(pdeck->actualcardid != -1) pdeck->table[pdeck->actualcardid]->is_render = false;
		pdeck->InsertCard(table[0], cardid);

		int showncardamount = pdeck->actualcardid / 6;

		int y = 30;
		int x = 105 + showncardamount * 2;

		table[0]->SetActualPos(x, y);
		table[0]->SetIdleState(x, y);

		delete [] table;
		table = NULL;

		amount = 0;
	}

	if(cardfromhandle && amount > 0)
	{
		table[0]->actual_pos = table[0]->idle_state;
		handles[handleid]->AddCard(table[0]);

		amount = 0;

		delete [] table;
		table = NULL;
	}

	if(cardfromldeck && amount > 0)
	{
		for(int i = 0; i < amount; i++)
		{
			table[i]->actual_pos = table[i]->idle_state;
		}

		pldeck[ldeckid]->AddCards(amount, table);

		amount = 0;

		delete [] table;
		table = NULL;
	}
}

void activity::LittleTakeCardsAction()
{
	for(int i = 0; i < 7; i++)
	{
		if(amount == 0 && pldeck[i]->cardcount > 0)
		{
			int id = 0;
			bool nothidden = false;

			for(; nothidden == false && id < pldeck[i]->cardcount; id++)
			{
				if(pldeck[i]->table[id]->hidden == false) nothidden = true;
			}

			id--;

			if(nothidden)
			{
				GetCursorPos(&startdragpos);
				ScreenToClient(child, &startdragpos);

				for(int j = id; j < pldeck[i]->cardcount; j++)
				{
					if(j == pldeck[i]->cardcount - 1 && startdragpos.x >= pldeck[i]->PosAndSizes.x && startdragpos.x <= pldeck[i]->PosAndSizes.x + 75 && startdragpos.y >= pldeck[i]->table[j]->actual_pos.y && startdragpos.y <= pldeck[i]->table[j]->actual_pos.y + 104)
					{
						amount = 1;

						cardfromdeck = false;
						cardfromhandle = false;
						cardfromldeck = true;

						ldeckid = i;

						table = new card * [1];

						table[0] = pldeck[i]->table[j];

						pldeck[i]->DeleteCards(1);

						break;
					}
					else if(j < pldeck[i]->cardcount - 1 && startdragpos.x >= pldeck[i]->PosAndSizes.x && startdragpos.x <= pldeck[i]->PosAndSizes.x + 75 && startdragpos.y >= pldeck[i]->table[j]->actual_pos.y && startdragpos.y < pldeck[i]->table[j+1]->actual_pos.y)
					{
						amount = pldeck[i]->cardcount - j;

						cardfromdeck = false;
						cardfromhandle = false;
						cardfromldeck = true;

						ldeckid = i;

						table = new card * [amount];

						for(int k = 0; k < amount; k++)
						{
							table[k] = pldeck[i]->table[j + k];
						}

						pldeck[i]->DeleteCards(amount);

						break;
					}
				}
			}
		}
	}
}

void activity::LittleShowCard()
{
	for(int i = 0; i < 7; i++)
	{
		GetCursorPos(&startdragpos);
		ScreenToClient(child, &startdragpos);

		if(pldeck[i]->cardcount > 0 && amount == 0 && pldeck[i]->table[pldeck[i]->cardcount - 1]->hidden == true && startdragpos.x >= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.x && startdragpos.x <= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.x + 75 && startdragpos.y >= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y && startdragpos.y <= pldeck[i]->table[pldeck[i]->cardcount - 1]->actual_pos.y + 104)
		{
			pldeck[i]->table[pldeck[i]->cardcount - 1]->hidden = false;
			pbackaction->AddAction(backaction::hidecard, &pldeck[i]->table[pldeck[i]->cardcount - 1], 1, backaction::null, backaction::null, 0, 0, 0, 0, 0);
		}
	}
}
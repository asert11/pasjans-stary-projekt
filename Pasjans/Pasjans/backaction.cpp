#include "backaction.h"

backaction::backaction()
{
	actionsleft = 0;
}

backaction::~backaction()
{
}

void backaction::AddAction(cardaction action, card ** source, int amount, addremove removefrom, addremove addto, int removeid, int addid, int idlex, int idley, int deckid)
{
	if(actionsleft == 10)
	{
		for(int i = 0; i < 9; i++)
		{
			table[i] = table[i + 1];
		}
		
		table[9].actionchoice = action;
		table[9].amount = amount;
		table[9].removefrom = removefrom;
		table[9].addto = addto;
		table[9].removeid = removeid;
		table[9].addid = addid;
		table[9].idlex = idlex;
		table[9].idley = idley;
		table[9].deckid = deckid;

		for(int j = 0; j < amount; j++)
		{
			table[9].cards[j] = source[j];
		}
	}
	else
	{
		table[actionsleft].actionchoice = action;
		table[actionsleft].amount = amount;
		table[actionsleft].removefrom = removefrom;
		table[actionsleft].addto = addto;
		table[actionsleft].removeid = removeid;
		table[actionsleft].addid = addid;
		table[actionsleft].idlex = idlex;
		table[actionsleft].idley = idley;
		table[actionsleft].deckid = deckid;

		for(int i = 0; i < amount; i++)
		{
			table[actionsleft].cards[i] = source[i];
		}
	}

	if(actionsleft != 10) actionsleft++;
}

void backaction::Back()
{
	if(actionsleft > 0)
	{
		switch(table[actionsleft - 1].actionchoice)
		{
		case hidecard:

			table[actionsleft - 1].cards[0]->hidden = true;
			break;

		case previousdeckcard:

			if(pdeck->actualcardid != -1)
			{
				pdeck->table[pdeck->actualcardid]->is_render = false;
				pdeck->actualcardid--;
			}
			else
			{
				pdeck->actualcardid = pdeck->cardcount - 1;
			}

			if(pdeck->actualcardid != -1)
			{
				pdeck->table[pdeck->actualcardid]->is_render = true;
			}

			break;

		case returncard:

			switch(table[actionsleft - 1].addto)
			{
			case handle:

				handles[table[actionsleft - 1].addid]->AddCard(table[actionsleft - 1].cards[0]);

				table[actionsleft - 1].cards[0]->actual_pos.x = table[actionsleft - 1].idlex;
				table[actionsleft - 1].cards[0]->idle_state.x = table[actionsleft - 1].idlex;

				table[actionsleft - 1].cards[0]->actual_pos.y = table[actionsleft - 1].idley;
				table[actionsleft - 1].cards[0]->idle_state.y = table[actionsleft - 1].idley;
				break;

			case deck:

				if(pdeck->actualcardid != -1) pdeck->table[pdeck->actualcardid]->is_render = false;
				pdeck->InsertCard(table[actionsleft - 1].cards[0], table[actionsleft - 1].deckid);

				table[actionsleft - 1].cards[0]->actual_pos.x = table[actionsleft - 1].idlex;
				table[actionsleft - 1].cards[0]->idle_state.x = table[actionsleft - 1].idlex;

				table[actionsleft - 1].cards[0]->actual_pos.y = table[actionsleft - 1].idley;
				table[actionsleft - 1].cards[0]->idle_state.y = table[actionsleft - 1].idley;
				break;

			case ldeck:

				pldeck[table[actionsleft - 1].addid]->AddCards(table[actionsleft - 1].amount, table[actionsleft - 1].cards);

				for(int i = 0; i < table[actionsleft - 1].amount; i++)
				{
					table[actionsleft - 1].cards[i]->actual_pos.x = table[actionsleft - 1].idlex;
					table[actionsleft - 1].cards[i]->idle_state.x = table[actionsleft - 1].idlex;

					table[actionsleft - 1].cards[i]->actual_pos.y = table[actionsleft - 1].idley + 16 * i;
					table[actionsleft - 1].cards[i]->idle_state.y = table[actionsleft - 1].idley + 16 * i;
				}

				break;
			}

			switch(table[actionsleft - 1].removefrom)
			{
			case handle:

				handles[table[actionsleft - 1].removeid]->DeleteCard();
				break;

			case ldeck:

				pldeck[table[actionsleft - 1].removeid]->DeleteCards(table[actionsleft - 1].amount);
				break;
			}

			break;
		}
		
		actionsleft--;
	}

	
}
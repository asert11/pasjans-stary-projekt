#include "card.h"

SDL_Texture * card::back = NULL;
SDL_Rect card::wymiary;
int card::cardcount = 0;
bool card::error = false;

bool card::iserror()
{
	return error;
}

card::card()
{
	if(cardcount == 0)
	{
		wymiary.w = 75;
		wymiary.h = 104;

		back = LoadTexture("cards\\back");

		if(back == NULL)
		{
			#ifdef CONSOLEOUTPUT
			printf("Cannot load back card image: %s\n", SDL_GetError());
			_getch();
			#endif
			error = true;
		}
	}

	std::stringstream count;

	count << "cards\\" << cardcount;

	front = LoadTexture(count.str().c_str());
	
	if(front == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load front card image: %s\n", SDL_GetError());
		_getch();
		#endif
		error = true;
	}

	color = cardcount / 13;
	value = cardcount % 13;
	hidden = false;
	drag = false;
	is_render = false;

	cardcount++;
}

card::~card()
{
	cardcount--;

	if(cardcount == 0)
	{
		SDL_DestroyTexture(back);		
	}

	SDL_DestroyTexture(front);
}

void card::Render()
{
	if(is_render)
	{
		actual_pos.w = wymiary.w;
		actual_pos.h = wymiary.h;

		if(isFrontShown())
		{
			SDL_RenderCopy(renderer, front, NULL, &actual_pos);
		}
		else
		{
			SDL_RenderCopy(renderer, back, NULL, &actual_pos);
		}
	}
}

void card::SetIdleState(int x, int y)
{
	idle_state.x = x;
	idle_state.y = y;
}

void card::SetActualPos(int x, int y)
{
	actual_pos.x = x;
	actual_pos.y = y;
}

void card::TurnCard()
{
	hidden = !hidden;
}

void card::LetDrag(bool dragable)
{
	drag = dragable;
}

void card::DrawCard(bool draw)
{
	is_render = draw;
}

SDL_Texture * card::LoadTexture(std::string file)
{
	SDL_Surface * temp = SDL_LoadBMP(file.c_str());

	if(temp == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load image: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	if(SDL_SetColorKey(temp, SDL_TRUE, SDL_MapRGB(temp->format, 0, 255, 255)) != 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot set color key: %s\n", SDL_GetError());
		_getch();
		#endif
		SDL_FreeSurface(temp);
		return NULL;
	}

	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FreeSurface(temp);

	if(texture == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create texture: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	return texture;
}
	
void card::GetIdleState(int & x, int & y)
{
	x = idle_state.x;
	y = idle_state.y;
}

void card::GetActualPos(int & x, int & y)
{
	x = actual_pos.x;
	y = actual_pos.y;
}
	
bool card::isFrontShown()
{
	if(hidden) return false;
	else return true;
}
	
void card::GetValueAndColor(int & cardcolor, int & cardvalue)
{
	cardcolor = color;
	cardvalue = value;
}

void card::GetSizes(int & w, int & h)
{
	w = wymiary.w;
	h = wymiary.h;
}

bool card::isDragable()
{
	if(drag) return true;
	else return false;
}
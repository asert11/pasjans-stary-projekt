#include "little_deck.h"

little_deck::little_deck(int x, int y)
{
	PosAndSizes.x = x;
	PosAndSizes.y = y;
	PosAndSizes.w = 75;
	PosAndSizes.h = 104;

	cardcount = 0;

	table = NULL;
}

little_deck::~little_deck()
{
	delete [] table;
}

void little_deck::GetPosition(int & x, int & y)
{
	x = PosAndSizes.x;
	y = PosAndSizes.y;
}

void little_deck::GetSizes(int & w, int & h)
{
	w = PosAndSizes.w;
	h = PosAndSizes.h;
}

void little_deck::AddCards(int amount, card ** wsk)
{
	card ** oldtab = table;

	table = new card * [cardcount + amount];

	for(int i = 0; i < cardcount; i++)
	{
		table[i] = oldtab[i];
	}

	for(int i = cardcount, j = 0; i < cardcount + amount; i++, j++)
	{
		table[i] = wsk[j];
	}

	delete [] oldtab;

	cardcount += amount;
}

void little_deck::DeleteCards(int amount)
{
	cardcount -= amount;

	if(cardcount == 0)
	{
		delete [] table;
		table = NULL;
	}
	else
	{
		card ** oldtab = table;

		table = new card * [cardcount];

		for(int i = 0; i < cardcount; i++)
		{
			table[i] = oldtab[i];
		}

		delete [] oldtab;
	}
}

void little_deck::Render()
{
	for(int i = 0; i < cardcount; i++)
	{
		table[i]->Render();
	}
}
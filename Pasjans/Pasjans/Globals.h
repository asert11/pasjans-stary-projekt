#ifndef GLOBALS_H
#define GLOBALS_H

#include "console.h"
#include <Windows.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include "resource.h"
#include <conio.h>
#include <SDL.h>
#include <SDL_syswm.h>
#include <string>
#include <sstream>
#include "card.h"
#include "handle.h"
#include "deck.h"
#include "little_deck.h"
#include <cstdlib>
#include <ctime>
#include "activity.h"
#include "backaction.h"
#include <SDL_mixer.h>

void CreateConsole();
LRESULT CALLBACK LowLevelKeyboardProc (INT nCode, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
bool InitWindow();
bool InitSDL();
void Render();
void QuitEverything();
card ** Randomize();
void Distribute();

extern HWND hwnd;
extern HWND child;
extern HMENU menu;

extern SDL_Window * window;
extern SDL_Renderer * renderer;
extern SDL_Texture * background;

extern handle * handles[4];
extern card * pcards[52];
extern deck * pdeck;
extern little_deck * pldeck[7];
extern activity * paction;
extern backaction * pbackaction;
extern bool GameStarted;
extern bool react;
extern Mix_Chunk * Clapping;

extern bool RenderRun;
extern bool quit;

#endif
#include "Globals.h"

void CreateConsole()
{
	AllocConsole();

    HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;
}

LRESULT CALLBACK LowLevelKeyboardProc (INT nCode, WPARAM wParam, LPARAM lParam)
{
    KBDLLHOOKSTRUCT *pkbhs = (KBDLLHOOKSTRUCT *) lParam;

	if(nCode == HC_ACTION && pkbhs->vkCode == VK_TAB && pkbhs->flags & LLKHF_ALTDOWN)
	{
		EnableWindow(hwnd, TRUE);

		POINT point;
		GetCursorPos(&point);
		SetCursorPos(point.x, point.y);
	}
	return CallNextHookEx (0, nCode, wParam, lParam);
}

bool InitWindow()
{
	LPSTR KlasaOkna = "Klasa Okienka";
    WNDCLASSEX wc;
    
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = GetModuleHandle(0);
    wc.hIcon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(IDI_MYICON));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL;
    wc.lpszMenuName = NULL;
    wc.lpszClassName = KlasaOkna;
    wc.hIconSm = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(IDI_MYSMICON));
    
    if(RegisterClassEx(&wc) == 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot register window class: %s\n", GetLastError());
		_getch();
		#endif
		return false;
	}

	menu = LoadMenu(GetModuleHandle(0), MAKEINTRESOURCE(IDR_MENU));

	if(menu == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load menu: %s\n", GetLastError());
		_getch();
		#endif
		return false;
	}

	int captionheight = GetSystemMetrics(SM_CYCAPTION);
	int menuheight = GetSystemMetrics(SM_CYMENU);
	int frameheight = GetSystemMetrics(SM_CYFIXEDFRAME);
	int framewidth = GetSystemMetrics(SM_CXFIXEDFRAME);

	int windowheight = captionheight + menuheight + frameheight * 2 + 480;
	int windowwidth = framewidth * 2 + 641;

	hwnd = CreateWindowEx( 0, KlasaOkna, "Pasjans", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX ,
    CW_USEDEFAULT, CW_USEDEFAULT, windowwidth, windowheight, NULL, menu, GetModuleHandle(0), NULL);

	if(hwnd == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create parent window: %s\n", GetLastError());
		_getch();
		#endif
		return false;
	}

	child = CreateWindowEx(0, KlasaOkna, "okienko", WS_CHILD | WS_VISIBLE, 0, 0, 641, 480, hwnd, 0, GetModuleHandle(0), 0);

	if(child == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create child window: %s\n", GetLastError());
		_getch();
		#endif
		return false;
	}

	return true;
}

bool InitSDL()
{
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot init SDL: %s\n", SDL_GetError());
		_getch();
		#endif
		return false;
	}

	window = SDL_CreateWindowFrom(child);

	if(window == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create SDL window: %s\n", SDL_GetError());
		_getch();
		#endif
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if(renderer == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create SDL renderer: %s\n", SDL_GetError());
		_getch();
		#endif
		return false;
	}

	SDL_Surface * surface = SDL_LoadBMP("background");

	if(surface == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load background: %s\n", SDL_GetError());
		_getch();
		#endif
		return false;
	}

	background = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

	if(background == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create background texture: %s\n", SDL_GetError());
		_getch();
		#endif

		return false;
	}

	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0)
    {
        #ifdef CONSOLEOUTPUT
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		_getch();
		#endif

		return false;
    }

	Clapping = Mix_LoadWAV("Clapping.wav");
    if( Clapping == NULL )
    {
        #ifdef CONSOLEOUTPUT
		printf( "Failed to load scratch sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
		_getch();
		#endif

		return false;
    }

	return true;
}

void Render()
{
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, background, NULL, NULL);

	for(int i = 0; i < 4; i++)
	{
		handles[i]->Render();
	}

	pdeck->Render();

	for(int i = 0; i < 7; i++)
	{
		pldeck[i]->Render();
	}

	paction->Render();

	SDL_RenderPresent(renderer);
}

void QuitEverything()
{
	Mix_FreeChunk( Clapping );
	SDL_DestroyTexture(background);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	DestroyWindow(child);
	DestroyMenu(menu);
	DestroyWindow(hwnd);
	Mix_Quit();
	SDL_Quit();
}

card ** Randomize()
{
	card ** randcard = new card * [52];
	card ** wsk = new card * [52];

	for(int i = 0; i < 52; i++)
	{
		randcard[i] = pcards[i];
	}

	srand(time(NULL));

	for(int i = 0, cardcount = 52; i < 52; i++)
	{
		int randnumb;
		randnumb = rand() % cardcount;
		wsk[i] = randcard[randnumb];

		cardcount--;

		if(cardcount > 0)
		{
			card ** temptable = new card * [cardcount];
			
			for(int j = 0; j < randnumb; j++)
			{
				temptable[j] = randcard[j];
			}

			for(int j = randnumb; j < cardcount; j++)
			{
				temptable[j] = randcard[j+1];
			}

			delete [] randcard;

			randcard = new card * [cardcount];

			for(int j = 0; j < cardcount; j++)
			{
				randcard[j] = temptable[j];
			}

			delete [] temptable;
		}
	}

	delete [] randcard;

	return wsk;
}

void Distribute()
{
	for(int i = 0; i < 4; i++)
	{
		int cardcount = handles[i]->cardcount;

		for(int j = 0; j < cardcount; j++)
		{
			handles[i]->DeleteCard();
		}
	}

	for(int i = 0; i < 7; i++)
	{
		if(pldeck[i]->cardcount > 0) pldeck[i]->DeleteCards(pldeck[i]->cardcount);
	}
	
	if(pdeck->cardcount > 0) pdeck->ClearDeck();

	for(int i = 0; i < 52; i++)
	{
		pcards[i]->hidden = false;
		pcards[i]->is_render = false;
	}

	card ** randcard = Randomize();

	for(int i = 0; i < 24; i++)
	{
		pdeck->AddCard(randcard[i]);
		randcard[i]->DrawCard(false);
	}

	for(int i = 0, count = 24; i < 7; i++)
	{
		for(int j = 0; j <= i; j++, count++)
		{
			pldeck[i]->AddCards(1, &randcard[count]);

			int x = 16;
				
			int y = 144;

			randcard[count]->DrawCard(true);
				
			randcard[count]->SetIdleState(x + i * 89, y + j * 3);

			randcard[count]->SetActualPos(x + i * 89, y + j * 3);

			if(j != i)
			{
				randcard[count]->TurnCard();
			}
			
		}
	}

	delete [] randcard;

	pbackaction->actionsleft = 0;

	react = true;
}
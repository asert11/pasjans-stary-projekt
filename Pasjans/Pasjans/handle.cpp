#include "handle.h"

SDL_Texture * handle::image = NULL;
SDL_Rect handle::sizes;
bool handle::error = false;
int handle::handlecount = 0;

handle::handle(int x, int y)
{
	if(handlecount == 0)
	{
		image = LoadTexture("cards\\handle");

		if(image == NULL)
		{
			#ifdef CONSOLEOUTPUT
			printf("Cannot load handle image: %s\n", SDL_GetError());
			_getch();
			#endif
			error = true;
		}

		sizes.w = 75;
		sizes.h = 104;
	}

	position.x = x;
	position.y = y;

	table = NULL;

	cardcount = 0;
	handlecount++;
}

handle::~handle()
{
	handlecount--;

	if(handlecount == 0)
	{
		SDL_DestroyTexture(image);
	}

	delete [] table;
}

int handle::GetValueOfLastCard()
{
	int value;
	int color;

	if(cardcount > 0)
	{
		table[cardcount - 1]->GetValueAndColor(color, value);
	}
	else value = -1;

	return value;
}

void handle::GetSizes(int & w, int & h)
{
	w = sizes.w;
	h = sizes.h;
}

bool handle::isError()
{
	return error;
}

void handle::GetPosition(int & x, int & y)
{
	x = position.x;
	y = position.y;
}

void handle::Render()
{
	position.w = sizes.w;
	position.h = sizes.h;

	if(cardcount == 0)
	{
		SDL_RenderCopy(renderer, image, NULL, &position);
	}
	else
	{
		table[cardcount - 1]->Render();
	}
}

void handle::AddCard(card * source)
{
	card ** oldtab = table;

	table = new card * [cardcount + 1];

	for(int i = 0; i < cardcount; i++)
	{
		table[i] = oldtab[i];
	}

	table[cardcount] = source;
	
	delete [] oldtab;

	cardcount++;
}

void handle::DeleteCard()
{
	cardcount--;

	if(cardcount == 0)
	{
		delete [] table;
		table = NULL;
	}
	else
	{
		card ** oldtab = table;

		table = new card * [cardcount];

		for(int i = 0; i < cardcount; i++)
		{
			table[i] = oldtab[i];
		}

		delete [] oldtab;
	}
}

SDL_Texture * handle::LoadTexture(std::string file)
{
	SDL_Surface * temp = SDL_LoadBMP(file.c_str());

	if(temp == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load image: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	if(SDL_SetColorKey(temp, SDL_TRUE, SDL_MapRGB(temp->format, 0, 255, 255)) != 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot set color key: %s\n", SDL_GetError());
		_getch();
		#endif
		SDL_FreeSurface(temp);
		return NULL;
	}

	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FreeSurface(temp);

	if(texture == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create texture: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	if(SDL_SetTextureAlphaMod(texture, 70) != 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot set texture alpha: %s\n", SDL_GetError());
		_getch();
		#endif
		SDL_DestroyTexture(texture);
		return NULL;
	}

	return texture;
}
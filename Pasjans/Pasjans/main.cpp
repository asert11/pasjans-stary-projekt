#include "Globals.h"

HWND hwnd = NULL;
HWND child = NULL;
HMENU menu = NULL;

SDL_Window * window = NULL;
SDL_Renderer * renderer = NULL;
SDL_Texture * background = NULL;

handle * handles[4];
card * pcards[52];
deck * pdeck;
little_deck * pldeck[7];
activity * paction;
backaction * pbackaction;
Mix_Chunk * Clapping = NULL;

bool RenderRun = false;
bool GameStarted = false;
bool quit = false;
bool react = false;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	#ifdef CONSOLEOUTPUT
	CreateConsole();
	#endif

	if(InitWindow() && InitSDL())
	{
		handle handle1(283, 30);
		handle handle2(372, 30);
		handle handle3(461, 30);
		handle handle4(550, 30);

		handles[0] = &handle1;
		handles[1] = &handle2;
		handles[2] = &handle3;
		handles[3] = &handle4;

		deck deck1;

		pdeck = &deck1;

		little_deck ldeck1(16, 144);
		little_deck ldeck2(105, 144);
		little_deck ldeck3(194, 144);
		little_deck ldeck4(283, 144);
		little_deck ldeck5(372, 144);
		little_deck ldeck6(461, 144);
		little_deck ldeck7(550, 144);

		pldeck[0] = &ldeck1;
		pldeck[1] = &ldeck2;
		pldeck[2] = &ldeck3;
		pldeck[3] = &ldeck4;
		pldeck[4] = &ldeck5;
		pldeck[5] = &ldeck6;
		pldeck[6] = &ldeck7;

		card cards[52];

		for(int i = 0; i < 52; i++)
		{
			pcards[i] = &cards[i];
		}

		activity action;

		paction = &action;

		backaction backsystem;

		pbackaction = &backsystem;

		Distribute();

		if(!handle::isError() && !card::iserror() && !deck::isError())
		{
			ShowWindow(hwnd, SW_SHOW);
			UpdateWindow(hwnd);

			RenderRun = true;
			GameStarted = true;

			SDL_Event e;

			SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);
	
			HHOOK hook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, GetModuleHandle(0), 0);

			while(!quit)
			{
				while(SDL_PollEvent(&e))
				{
					if(e.type == SDL_SYSWMEVENT)
					{
						action.event(e);

						switch(e.syswm.msg->msg.win.msg)
						{
						case WM_DESTROY:

							quit = true;
							break;

						case WM_LBUTTONDOWN:

							EnableWindow(hwnd, FALSE);
							break;
						}
					}
				}

				if(!(GetKeyState(VK_LBUTTON) & 0x8000))
				{
					EnableWindow(hwnd, TRUE);

					POINT point;
					GetCursorPos(&point);
					SetCursorPos(point.x, point.y);

					action.DropCard();
				}

				if(GetActiveWindow() != hwnd)
				{
					EnableWindow(hwnd, TRUE);

					POINT point;
					GetCursorPos(&point);
					SetCursorPos(point.x, point.y);
					
					action.ReturnCard();
				}

				action.Move();

				if(handles[0]->GetValueOfLastCard() == 12 && handles[1]->GetValueOfLastCard() == 12 && handles[2]->GetValueOfLastCard() == 12 && handles[3]->GetValueOfLastCard() == 12)
				{
					if(react)
					{
						Mix_PlayChannel( -1, Clapping, 0 );
					}

					react = false;
				}

				Render();
			}

			GameStarted = false;

			UnhookWindowsHookEx(hook);
		}
	}

	QuitEverything();

	return 0;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_UNINITMENUPOPUP:

		if(RenderRun) Render();
		return 0;
		break;

	case WM_MOVE:

		if(RenderRun) Render();
		return 0;
		break;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDM_QUIT:

			quit = true;
			return 0;
			break;

		case IDM_UNDO:

			if(GameStarted && react) pbackaction->Back();
			return 0;
			break;

		case IDM_DISTRIBUTE:

			if(GameStarted) Distribute();
			return 0;
			break;
		}
		break;
	}

	return DefWindowProc( hwnd, msg, wParam, lParam );
}
#include "deck.h"

SDL_Texture * deck::frontdecktexture = NULL;
SDL_Texture * deck::backdecktexture = NULL;
SDL_Texture * deck::lackofcards = NULL;
bool deck::error = false;

bool deck::isError()
{
	return error;
}

deck::deck()
{
	table = NULL;
	cardcount = 0;
	position.x = 16;
	position.y = 30;
	actualcardid = -1;

	frontdecktexture = LoadTexture("cards\\front");

	if(frontdecktexture == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load front deck image: %s\n", SDL_GetError());
		_getch();
		#endif
		error = true;
	}

	backdecktexture = LoadTexture("cards\\back");

	if(backdecktexture == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load back deck image: %s\n", SDL_GetError());
		_getch();
		#endif
		error = true;
	}

	lackofcards = LoadTexture("cards\\handle");

	if(lackofcards == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load lack of cards deck image: %s\n", SDL_GetError());
		_getch();
		#endif
		error = true;
	}

	SDL_SetTextureAlphaMod(lackofcards, 70);
}

deck::~deck()
{
	delete [] table;

	SDL_DestroyTexture(frontdecktexture);
	SDL_DestroyTexture(backdecktexture);
	SDL_DestroyTexture(lackofcards);
}

void deck::Render()
{
	if(cardcount > 0)
	{
		if(actualcardid == -1)
		{
			int hiddencardamount = (cardcount + 5) / 6;

			position.x = 14;
			position.y = 30;
			position.w = 75;
			position.h = 104;

			for(int i = 0; i < hiddencardamount; i++)
			{
				position.x += 2;
				SDL_RenderCopy(renderer, backdecktexture, NULL, &position);
			}
		}
		else
		{
			int hiddencardamount = (cardcount - actualcardid + 4) / 6;
			 
			position.x = 16;
			position.y = 30;
			position.w = 75;
			position.h = 104;

			if(hiddencardamount == 0) SDL_RenderCopy(renderer, lackofcards, NULL, &position);

			position.x = 14;

			for(int i = 0; i < hiddencardamount; i++)
			{
				position.x += 2;
				SDL_RenderCopy(renderer, backdecktexture, NULL, &position);
			}

			int showncardamount = actualcardid / 6;

			position.x = 103;
			position.y = 30;
			position.w = 3;
			position.h = 104;

			for(int i = 0; i < showncardamount; i++)
			{
				position.x += 2;
				SDL_RenderCopy(renderer, frontdecktexture, NULL, &position);
			}

			table[actualcardid]->Render();
		}
	}
	else
	{
		position.x = 16;
		position.y = 30;
		position.w = 75;
		position.h = 104;

		SDL_RenderCopy(renderer, lackofcards, NULL, &position);
	}
}

void deck::AddCard(card * source)
{
	card ** oldtab = table;

	table = new card * [cardcount + 1];

	for(int i = 0; i < cardcount; i++)
	{
		table[i] = oldtab[i];
	}

	table[cardcount] = source;

	delete [] oldtab;

	cardcount++;
}

void deck::InsertCard(card * source, int id)
{
	card ** oldtab = table;

	table = new card * [cardcount + 1];

	for(int i = 0; i < id; i++)
	{
		table[i] = oldtab[i];
	}

	table[id] = source;

	for(int i = id + 1; i < cardcount + 1; i++)
	{
		table[i] = oldtab[i - 1];
	}

	delete [] oldtab;

	actualcardid = id;
	cardcount++;
}

void deck::RemoveCard()
{
	cardcount--;

	card ** oldtab = table;

	table = new card * [cardcount];

	for(int i = 0; i < actualcardid; i++)
	{
		table[i] = oldtab[i];
	}

	for(int i = actualcardid + 1; i < cardcount + 1; i++)
	{
		table[i-1] = oldtab[i];
	}

	actualcardid--;

	delete [] oldtab;
}

void deck::NextCard()
{
	if(actualcardid == cardcount - 1) actualcardid = -1;
	else actualcardid++;
}

SDL_Texture * deck::LoadTexture(std::string file)
{
	SDL_Surface * temp = SDL_LoadBMP(file.c_str());

	if(temp == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot load image: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	if(SDL_SetColorKey(temp, SDL_TRUE, SDL_MapRGB(temp->format, 0, 255, 255)) != 0)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot set color key: %s\n", SDL_GetError());
		_getch();
		#endif
		SDL_FreeSurface(temp);
		return NULL;
	}

	SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FreeSurface(temp);

	if(texture == NULL)
	{
		#ifdef CONSOLEOUTPUT
		printf("Cannot create texture: %s\n", SDL_GetError());
		_getch();
		#endif
		return NULL;
	}

	return texture;
}

void deck::ClearDeck()
{
	delete [] table;
	table = NULL;

	cardcount = 0;
	actualcardid = -1;
}
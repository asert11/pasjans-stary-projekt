#ifndef LITTLE_DECK_H
#define LITTLE_DECK_H

#include "console.h"
#include "card.h"

class little_deck
{
public:

	little_deck(int x, int y);
	~little_deck();
	void AddCards(int amount, card ** wsk);
	void DeleteCards(int amount);
	void Render();
	void GetPosition(int & x, int & y);
	void GetSizes(int & w, int & h);

private:

	SDL_Rect PosAndSizes;
	int cardcount;
	card ** table;

	friend class activity;
	friend void Distribute();
};

#endif
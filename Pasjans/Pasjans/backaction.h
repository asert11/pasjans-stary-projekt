#ifndef BACKACTION_H
#define BACKACTION_H

#include "console.h"
#include "card.h"
#include "deck.h"
#include "handle.h"
#include "little_deck.h"

extern deck * pdeck;
extern handle * handles[4];
extern little_deck * pldeck[7];

class backaction
{
public:

	enum cardaction
	{
		hidecard,
		previousdeckcard,
		returncard
	};

	enum addremove
	{
		deck,
		ldeck,
		handle,
		null
	};

	backaction();
	~backaction();
	void AddAction(cardaction action, card ** source, int amount, addremove removefrom, addremove addto, int removeid, int addid, int idlex, int idley, int deckid);
	void Back();
	
private:

	struct action
	{
		cardaction actionchoice;
		card * cards[13];
		int amount;
		addremove removefrom;
		addremove addto;
		int removeid;
		int addid;
		int idlex;
		int idley;
		int deckid;
	};

	action table[10];

	int actionsleft;

	friend void Distribute();
};

#endif
#ifndef ACTIVITY_H
#define ACTIVITY_H

#include "console.h"
#include "card.h"
#include <SDL_syswm.h>
#include <Windows.h>
#include "deck.h"
#include "handle.h"
#include "little_deck.h"
#include "backaction.h"

extern handle * handles[4];
extern card * pcards[52];
extern deck * pdeck;
extern little_deck * pldeck[7];
extern backaction * pbackaction;
extern HWND child;
extern bool react;

class activity
{
public:

	activity();
	~activity();
	void event(SDL_Event e);
	void Render();
	void Move();
	void DropCard();
	void ReturnCard();

private:

	void DeckNextCardAction();
	void DeckTakeCardAction();
	void HandleTakeCardAction();
	void LittleTakeCardsAction();
	void LittleShowCard();

	POINT point, startdragpos;
	card ** table;
	bool cardfromdeck;
	bool cardfromhandle;
	bool cardfromldeck;
	int handleid;
	int cardid;
	int ldeckid;
	int amount;
};

#endif
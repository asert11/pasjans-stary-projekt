#ifndef CARD_H
#define CARD_H

#include "console.h"
#include <string>
#include <SDL.h>
#include <conio.h>
#include <sstream>

extern SDL_Renderer * renderer;

class card
{
public:

	card();
	~card();
	void Render();
	void SetIdleState(int x, int y);
	void SetActualPos(int x, int y);
	void GetIdleState(int & x, int & y);
	void GetActualPos(int & x, int & y);
	void TurnCard();
	bool isFrontShown();
	void LetDrag(bool dragable);
	void DrawCard(bool draw);
	void GetValueAndColor(int & cardcolor, int & cardvalue);
	void GetSizes(int & w, int & h);
	bool isDragable();

	static bool iserror();

private:

	static int cardcount;
	static SDL_Texture * back;
	static SDL_Rect wymiary;
	static bool error;
	SDL_Texture * front;
	int color;
	int value;
	bool hidden;
	bool drag;
	SDL_Rect idle_state;
	SDL_Rect actual_pos;
	bool is_render;

	SDL_Texture * LoadTexture(std::string file);

	friend class activity;
	friend class backaction;
	friend void Distribute();
};

#endif
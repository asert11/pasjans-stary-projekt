#ifndef HANDLE_H
#define HANDLE_H

#include "card.h"
#include "console.h"

class handle
{
public:

	handle(int x, int y);
	~handle();
	void GetSizes(int & w, int & h);
	static bool isError();
	void GetPosition(int & x, int & y);
	void Render();
	void AddCard(card * source);
	void DeleteCard();
	int GetValueOfLastCard();

private:

	static SDL_Texture * image;
	static SDL_Rect sizes;
	static bool error;
	static int handlecount;
	SDL_Rect position;
	card ** table;
	int cardcount;

	SDL_Texture * LoadTexture(std::string file);

	friend class activity;
	friend void Distribute();
};

#endif